# Smash Rewards Program
Smash Labs Projects is creating the internal rewards program for customer and employees.
The main idea for the application is that customers can assign points to their employees that can be redemeed later for items on the Smash Rewards Catalog.
Points will be translated to a dollar value for items redemption.

## User points microservice
On this microservice we want to develop the following services:
1. Add points to user
2. Remove points from user
3. Get points by user

## Directions
Please use the technology stack that you feel more confortable with i.e. Java, Spring, Rest, MongoDB or Java, MongoDB and Event sourcing, etc.
If you have any cloud provider knowledge or devops knowledge under your belt, feel free to also create the insfrastructure nor generate the script/cloudformation, etc to generate it
Please provide any startup/create script for your data store if needed
Please use your engineering skills to not only generate a working solution but try to create a solution that is prone to changes and easy to extend. Also try to catch any possible business rules that you might think are appropiate for this microservice i.e: Points cannot be strings, somewhow communicate that to the consumer.

## Outcome
- Codebase
- Please document your solution as much as possible, because you have the freedom to choose any tech stack, we need to be able to pick up where you left, this means other developers need to collaborate with your solution. Documenation should include any contracts, db scripts(sql scripts, liquibase if code first, etc), configuration, how to deploy, etc.

## Suggested data contract for points
| Prop | Type |
| --- | ----------- |
| UserId | String |
| Points | Number |


### The idea is that you use 5-7 hours to create as much as possible, if you need to use more then please let us know


